from os import getenv
from dotenv import load_dotenv

load_dotenv()

DEBUG = getenv("DEBUG", False)
JWT_ACCESS_SECRET = getenv("JWT_ACCESS_SECRET")
JWT_REFRESH_SECRET = getenv("JWT_REFRESH_SECRET")
ALLOWED_ORIGIN = getenv("ALLOWED_ORIGIN")
MONGO_URI = getenv("MONGO_URI")
