import werkzeug
from flask import Blueprint

werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import Api
from .auth import auth_ns
from .books import books_ns

blueprint = Blueprint("api", __name__)

api = Api(blueprint,
          version="1.0",
          title="Book Manager",
          description="App for managing favourite books.")

api.add_namespace(auth_ns)
api.add_namespace(books_ns)
