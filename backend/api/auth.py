import re
import werkzeug
from flask import request, make_response
from passlib.hash import pbkdf2_sha256
from util import *

werkzeug.cached_property = werkzeug.utils.cached_property  # Fixes a bug with the werkzeug package
from flask_restplus import Resource, Namespace

auth_ns = Namespace(name="Authentication",
                    description="Handles registering and logging users.",
                    path="/")


@auth_ns.route("/register")
class Register(Resource):
    @auth_ns.doc(responses={200: "OK",
                            400: "Invalid username or password.",
                            409: "Username already exists."},
                 description="Registers the user.",
                 params={"username": {"description": "Username to register with.",
                                      "in": "formData",
                                      "type": "string",
                                      "required": True},
                         "password": {"description": "Password to register with.",
                                      "in": "formData",
                                      "type": "string",
                                      "required": True}
                         })
    def post(self):
        username = request.form["username"]
        password = request.form["password"]
        if not username_exists(username):
            if not validate_username(username):
                return response(400, "Username must have 3-20 characters.")
            if not validate_password(password):
                return response(400,
                                "Password must have at least 8 characters, at least one number, at least one "
                                "uppercase and lowercase letter and can contain any of the characters "
                                "!@%&*_+=?:+")
            mongo.db.users.insert({"username": username, "password": pbkdf2_sha256.hash(password), "books": []})
            return response()
        else:
            return response(409, "This username already exists")


@auth_ns.route("/login")
class Login(Resource):
    @auth_ns.doc(responses={200: "OK",
                            401: "Username and password combination is not correct."},
                 description="Logs in the user.",
                 params={"username": {"description": "Username to login with.",
                                      "in": "formData",
                                      "type": "string",
                                      "required": True},
                         "password": {"description": "Password to login with.",
                                      "in": "formData",
                                      "type": "string",
                                      "required": True}})
    def post(self):
        return verify_login(request.form["username"], request.form["password"])
    

@auth_ns.route("/logout")
class Logout(Resource):
    @auth_ns.doc(responses={200: "OK"},
                 description="Logs out the user.")
    def post(self):
        res = make_response(response())
        res.set_cookie("access_token", "", expires=0)
        res.set_cookie("refresh_token", "", expires=0)
        return res


def validate_username(username):
    return not (username is None or not 3 <= len(username) <= 20)


def validate_password(password):
    if password is None or len(password) < 8:
        return False
    regex = r'^.*(?=.{8,10})(?=.*[a-zA-Z])(?=.*?[A-Z])(?=.*\d)[a-zA-Z0-9!@%&*_+=?:]+$'
    return re.search(regex, password)


def verify_login(username, password):
    user = mongo.db.users.find_one({"username": username})
    if user is None or not pbkdf2_sha256.verify(password, user["password"]):
        return response(401, "Username and password combination is not correct.")
    access_token = generate_token(username)
    refresh_token = generate_token(username, True)
    res = make_response(response(data={"username": username}))
    res.set_cookie("access_token", access_token, httponly=True, samesite="None", secure=True)
    res.set_cookie("refresh_token", refresh_token, httponly=True, samesite="None", secure=True)
    return res


def username_exists(username):
    return mongo.db.users.find_one({"username": username}) is not None
