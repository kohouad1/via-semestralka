from functools import wraps
import requests
from flask import request, g
from flask_restplus import Namespace, fields, Resource
from util import *

books_ns = Namespace(name="Book operations",
                     description="Provides operations modifying user's book list.",
                     path="/books")


def jwt_required(endpoint):
    @wraps(endpoint)
    def check_for_jwt(*args, **kwargs):
        access_token = request.cookies.get("access_token")
        if access_token is None:
            return response(401, "Authentication access token not provided.")
        try:
            g.jwt_data = verify_token(access_token)
            return endpoint(*args, **kwargs)
        except jwt.PyJWTError as e:
            if not isinstance(e, jwt.ExpiredSignatureError):
                return response(401, "Invalid access token.")
        
        refresh_token = request.cookies.get("refresh_token")
        if refresh_token is None:
            return response(401, "Authentication refresh token not provided.")
        try:
            g.jwt_data = verify_token(refresh_token, True)
            return endpoint(*args, **kwargs)
        except jwt.ExpiredSignatureError:
            return response(401, "Refresh token has expired.")
        except jwt.PyJWTError as e:
            return response(401, "Authentication with refresh token has failed: " + str(e))
    
    return check_for_jwt


@books_ns.route("/<gbid>")
@books_ns.param("gbid", "Google Books ID of the book.", required=True)
class BookManipulator(Resource):
    @books_ns.doc(responses={201: "OK",
                             404: "Book with the supplied ID does not exist in Google Books service.",
                             409: "Book already exists in the user's list.",
                             422: "Invalid category name."},
                  description="Adds a book with the given Google Books ID to the user's list. If the category is not "
                              "specified, it is assigned to the TO_READ category.",
                  params={"category": {"description": "Category to assign to the book.",
                                       "in": "query",
                                       "type": "string"}})
    @jwt_required
    def post(self, gbid):
        """ Adds a book to the user's list. """
        username = g.jwt_data["user"]
        
        if exists_in_book_list(username, gbid):
            return response(409, "Book already exists in the user's list.")
        
        result = requests.get(f"https://www.googleapis.com/books/v1/volumes/{gbid}")
        if result.status_code == 404:
            return response(404, "Book with the supplied ID does not exist in Google Books service.")
        
        book = construct_book_dict_from_response(result.json())
        category = request.args.get("category")
        if category is None:
            add_book_to_user_list(username, book)
            return response(201)
        
        if category not in VALID_CATEGORIES:
            return response(422, f"Invalid category name. Valid values are {str(VALID_CATEGORIES)}.")
        
        add_book_to_user_list(username, book, category)
        return response(201)
    
    @books_ns.doc(responses={200: "OK",
                             400: "Category name must be provided.",
                             404: "Book does not exist in the user's list.",
                             422: "Invalid category name."},
                  description="Updates a book's assigned category.",
                  params={"category": {"description": "Category newly assigned to the book.",
                                       "in": "query",
                                       "type": "string",
                                       "required": True}})
    @jwt_required
    def patch(self, gbid):
        """ Updates a book's assigned category. """
        username = g.jwt_data["user"]
        
        if not exists_in_book_list(username, gbid):
            return response(404, "Book does not exist in the user's list.")
        
        category = request.args.get("category")
        if category is None:
            return response(400, "Category name must be provided.")
        
        if category not in VALID_CATEGORIES:
            return response(422, f"Invalid category name. Valid values are {str(VALID_CATEGORIES)}.")
        
        mongo.db.users.update({"username": username,
                               "books": {"$elemMatch": {"gbid": gbid}}},
                              {"$set": {"books.$.category": category}})
        return response()
    
    @books_ns.doc(responses={200: "OK",
                             404: "Book does not exist in the user's list."},
                  description="Removes a book with the given Google Books ID from the user's list.")
    @jwt_required
    def delete(self, gbid):
        """ Removes a book from the user's list. """
        username = g.jwt_data["user"]
        
        if not exists_in_book_list(username, gbid):
            return response(404, "Book does not exist in the user's list.")
        
        mongo.db.users.update({"username": username},
                              {"$pull": {"books": {"gbid": gbid}}})
        return response()


@books_ns.route("/")
class BookReader(Resource):
    @books_ns.doc(responses={200: "OK",
                             422: "Invalid category name."},
                  description="Gets the list of all user's books. If the 'category' parameter is specified, "
                              "only books with that assigned category are returned.",
                  params={"category": {"description": "Assigned category to filter the books by.",
                                       "in": "query",
                                       "type": "string"}})
    @jwt_required
    def get(self):
        """ Returns the list of user's books. """
        username = g.jwt_data["user"]
        
        category = request.args.get("category")
        
        if category is not None and category not in VALID_CATEGORIES:
            return response(422, f"Invalid category name. Valid values are {str(VALID_CATEGORIES)}.")
        
        books = mongo.db.users.aggregate([
            {"$match": {"username": username}},
            {"$unwind": "$books"},
            {"$match": {"books.category": category if category else {"$in": VALID_CATEGORIES}}},
            {"$replaceRoot": {"newRoot": "$books"}}
        ])
        return response(data=list(books))

