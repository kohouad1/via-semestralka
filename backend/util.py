from flask import current_app, make_response
import datetime
import jwt
from db import mongo

VALID_CATEGORIES = ["TO_READ", "READING_NOW", "HAVE_READ"]


def response(status=200, message="OK", data=None):
    if data is None:
        data = []
    res = make_response({
        "message": message,
        "data": data,
    }, status)
    return res


def generate_token(username, is_refresh=False):
    expires_in = datetime.timedelta(days=1) if is_refresh else datetime.timedelta(minutes=15)
    secret = current_app.config["JWT_REFRESH_SECRET"] if is_refresh else current_app.config["JWT_ACCESS_SECRET"]
    return jwt.encode({"user": username,
                       "exp": datetime.datetime.utcnow() + expires_in},
                      secret)


def verify_token(token, is_refresh=False):
    payload = jwt.decode(token,
                         current_app.config["JWT_REFRESH_SECRET"] if is_refresh else
                         current_app.config["JWT_ACCESS_SECRET"])
    return payload


def construct_book_dict_from_response(request):
    return {"gbid": request["id"],
            "title": request["volumeInfo"]["title"],
            "authors": request["volumeInfo"]["authors"],
            "thumbnailUrl": request["volumeInfo"]["imageLinks"]["smallThumbnail"]}


def exists_in_book_list(username, gbid):
    return mongo.db.users.find_one({"username": username,
                                    "books": {"$elemMatch": {"gbid": {"$eq": gbid}}}})\
           is not None


def add_book_to_user_list(username, book, category="TO_READ"):
    book["category"] = category
    mongo.db.users.update({"username": username},
                          {"$push": {"books": book}})
