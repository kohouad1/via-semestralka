from flask import Flask
from flask_cors import CORS
from api.apis import blueprint
from db import mongo

app = Flask(__name__)
app.config.from_object("config")  # Load config variables

mongo.init_app(app)

# Enable CORS for all requests coming from the client. Includes also preflight requests.
CORS(app, resources={r"/*": {"origins": app.config["ALLOWED_ORIGIN"]}}, supports_credentials=True)
app.register_blueprint(blueprint)

if __name__ == '__main__':
    if app.config["DEBUG"]:
        app.run(ssl_context=("localhost-cert.pem", "localhost-key.pem"))
    else:
        app.run()
